classdef DoBot_Real < handle
    properties
        model;
    end
    methods
        
        function self = DoBot_Real(base)

            self.Getdobotmagician(base);
            
        end
        %% Getdobot
        % Given a name (optional), create and return a dobot robot model
        function Getdobotmagician(self,base)
        %     if nargin < 1
                % Create a unique name (ms timestamp after 1ms pause)
                pause(0.001);
                name = ['Dobot Magician',datestr(now,'yyyymmddTHHMMSSFFF')];
        %     end

            %L1 = Link('d',0.051,'a',0,      'alpha',0,              'offset',0);
            L1 = Link('d',0,'a',0,      'alpha',-pi/2,      'offset',-pi/2,     'qlim',[deg2rad(-90), deg2rad(90)]);
            L2 = Link('d',0,    'a',0.135,  'alpha',0,         'offset',-pi/2, 'qlim',[deg2rad(0), deg2rad(85)]);
            L3 = Link('d',0,    'a',0.147,  'alpha',0,          'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
            L4 = Link('d',0,    'a',0.060,  'alpha',pi/2,          'offset',-pi/2,     'qlim',[deg2rad(-90), deg2rad(95)]);
            L5 = Link('d',-0.072,    'a',0,  'alpha',0,          'offset',0, 'qlim',[deg2rad(-90),deg2rad(90)]);

            %L1 = Link('d',0.137,'a',0,'alpha',-pi/2,'offset',0,'qlim', deg2rad([-135 135]));
            %L2 = Link('d',0,'a',0.1393,'alpha',0,'offset',-pi/2, 'qlim', deg2rad([5 80]));     
            %L3 = Link('d',0,'a',0.16193,'alpha',0,'offset',0, 'qlim', deg2rad([15 170])); %pi/2 –q2 actual offset    
            %L4 = Link('d',0,'a',0.0597,'alpha',pi/2,'offset',-pi/2, 'qlim', [-pi/2 pi/2]);     

            self.model = SerialLink([L1 L2 L3 L4 L5],'name',name);
            self.model.base = base;
        end
        
        function GoToCartesian(self,x,y,z,r)
            cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos');
            cartmsg_ = rosmessage(cartsvc_);
            cartmsg_.TargetPoints=[x,y,z,r];
            cartsvc_.call(cartmsg_);
        end  
        
        function GoToJointAngles(self,q1,q2,q3,q4)
            jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles');
            jangmsg_ = rosmessage(jangsvc_);
            jangmsg_.TargetPoints = [ q1, q2, q3, q4 ];
            jangsvc_.call(jangmsg_);
        end
        
        function SetSuctionCup(self,state)
            suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
            suctioncupmsg_ = rosmessage(suctioncupsvc_);
            suctioncupmsg_.IsEndEffectorEnabled=1;
            suctioncupmsg_.EndEffectorState=state;
            suctioncupsvc_.call(suctioncupmsg_);
        end
        
        function CurrentJointState(q)
            sub = rossubscriber('/dobot_magician/joint_states');
            pause(1);
            %receive and print latest state message
            disp 'Receiving and printing latest state message ...'
            statemsg_ = receive(sub,10); %timeout after 10 seconds
            disp (statemsg_.Position)
        end
        
        function q=ikine(self,coord,yaw)
            rpy=tr2rpy(coord);
            x=coord(1,4);
            y=coord(2,4);
            z=coord(3,4);
            a1=self.model.links(1,1).d;
            a2=self.model.links(1,2).a;
            a3=self.model.links(1,3).a;
            a4=self.model.links(1,4).a;
            a5=abs(self.model.links(1,5).d);
            z4=z+a5;
            
            
    %        if (x>=0&&y>=0)
    %            q1=(atan(x/y)+pi)
    %        elseif (x<0&&y>=0)
    %            q1=(atan(x/y)+(pi/2))
    %        elseif (x>=0&&y<0)
    %            q1=-(atan(x/y))
    %        elseif (x<0&&y<0)
    %            q1=-(atan(x/y))
    %        end
            q1=atan(y/x);
            if y==0
               q1=0;
            end
            if(x>=0)
               x4=x-(cos(q1)*a4);
            elseif(x<0)
               x4=x+(sin(abs(q1))*a4);
            end
            if(y>=0)
                y4=y-(sin(q1)*a4);
            elseif(y<0)
                y4=y-(sin(q1)*a4);
            end
            l=(x4^2+y4^2)^(1/2);
            v=z4-a1;
            D=(l^2+v^2)^(1/2);
            t1=atan(v/l);
            t2=acos(((a2^2)+(D^2)-(a3^2))/(2*a2*D));
            alpha=t1+t2;
            beta=acos(((a2^2)+(a3^2)-(D^2))/(2*a2*a3));
            %standardikine=self.model.ikcon(coord)
            %q1=standardikine(1)
            q2=(pi()/2)-alpha;
            q3real=pi()-beta-alpha;
            q3model=pi/2-q2+q3real;
            q4=-q1+yaw;
            %q5=rpy(3);
            q=[q1,q2,q3real,q4];
        end
        
        function GoToXYZ(self,x,y,z,r)
            q=self.ikine(transl(x,y,z),r);
            %self.CurrentJointState()
            self.GoToJointAngles(q(1),q(2),q(3),q(4));
        end
    end
end
