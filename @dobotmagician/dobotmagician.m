classdef dobotmagician < handle
    properties
        %> Robot model
        model;
        logCount=1;
        endefectorofset=transl(.061,0,.047);
        
              
      
    end
    
methods%% Class for dobot simulation
    function self = dobotmagician(base,workspace)

    %> Define the boundaries of the workspace

    % robot = 
    self.Getdobotmagician(base);
    % robot = 
    self.PlotAndColourRobot(workspace); %robot,workspace);
    end

    %% Getdobot
    % Given a name (optional), create and return a dobot robot model
    function Getdobotmagician(self,base)
    %     if nargin < 1
            % Create a unique name (ms timestamp after 1ms pause)
            pause(0.001);
            name = ['Dobot Magician',datestr(now,'yyyymmddTHHMMSSFFF')];
    %     end

        %L1 = Link('d',0.051,'a',0,      'alpha',0,              'offset',0);
        L1 = Link('d',0.14,'a',0,      'alpha',-pi/2,      'offset',-pi/2,     'qlim',[deg2rad(-90), deg2rad(90)]);
        L2 = Link('d',0,    'a',0.135,  'alpha',0,         'offset',-pi/2, 'qlim',[deg2rad(0), deg2rad(85)]);
        L3 = Link('d',0,    'a',0.147,  'alpha',0,          'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        L4 = Link('d',0,    'a',0.060,  'alpha',pi/2,          'offset',-pi/2,     'qlim',[deg2rad(-90), deg2rad(95)]);
        L5 = Link('d',-0.072,    'a',0,  'alpha',0,          'offset',0, 'qlim',[deg2rad(-90),deg2rad(90)]);
         
        self.model = SerialLink([L1 L2 L3 L4 L5],'name',name);
        self.model.base = base;
    end
    %% Prereq
    function prereq(self,q)
        q5=0;
        self.model.plot([q(1,1:4),q5])
        for i=1:1:338
            q3model=(pi/2)-q(i,2)+q(i,3)
            q4=pi-q(i,2)-q3model
            self.model.animate([q(i,1),q(i,2),q3model,q4,q5])
            drawnow
        end         
    end
    
    %% Ikine()
    
 
   function q=ikine(self,coord)
        rpy=tr2rpy(coord)
        x=coord(1,4)
        y=coord(2,4)
        z=coord(3,4)
        a1=self.model.links(1,1).d
        a2=self.model.links(1,2).a
        a3=self.model.links(1,3).a
        a4=self.model.links(1,4).a
        a5=abs(self.model.links(1,5).d)
        z4=z+a5
%        if (x>=0&&y>=0)
%            q1=(atan(x/y)+pi)
%        elseif (x<0&&y>=0)
%            q1=(atan(x/y)+(pi/2))
%        elseif (x>=0&&y<0)
%            q1=-(atan(x/y))
%        elseif (x<0&&y<0)
%            q1=-(atan(x/y))
%        end
            q1=atan(y/x);
            if y==0
               q1=0;
            end
            if(x>=0)
               x4=x-(cos(q1)*a4);
            elseif(x<0)
               x4=x+(sin(abs(q1))*a4);
            end
            if(y>=0)
                y4=y-(sin(q1)*a4);
            elseif(y<0)
                y4=y-(sin(q1)*a4);
            end
        l=(x4^2+y4^2)^(1/2)
        v=z4-a1
        D=(l^2+v^2)^(1/2)
        t1=atan(v/l)
        t2=acos(((a2^2)+(D^2)-(a3^2))/(2*a2*D))
        alpha=t1+t2
        beta=acos(((a2^2)+(a3^2)-(D^2))/(2*a2*a3))
        %standardikine=self.model.ikcon(coord)
        %q1=standardikine(1)
        q2=(pi()/2)-alpha
        q3real=pi()-beta-alpha
        q3model=pi/2-q2+q3real
        q4=pi-q2-q3model %-((pi()/2)+((pi()/2)-t1)+((pi())-t2-beta)-(pi()))
        q5=rpy(3)
        q=[q1,q2,q3model,q4,q5]
    end
    
    %% PlotAndColourRobot
    % Given a robot index, add the glyphs (vertices and faces) and
    % colour them in if data is available 
    function PlotAndColourRobot(self,workspace)%robot,workspace)
        for linkIndex = 0:self.model.n

                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['dobotLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
            self.model.faces{linkIndex+1} = faceData;
            self.model.points{linkIndex+1} = vertexData;
        end

        % Display robot
        self.model.plot3d([0,pi/6,pi/2,pi/3,0],'noarrow', 'delay' , 0); %'workspace',workspace,
        if isempty(findobj(get(gca,'Children'),'Type','Light'))
            camlight
        end  
        self.model.delay = 0;

        % Try to correctly colour the arm (if colours are in ply file data)
        for linkIndex = 0:self.model.n
            handles = findobj('Tag', self.model.name);
            h = get(handles,'UserData');
            try 
                h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                              , plyData{linkIndex+1}.vertex.green ...
                                                              , plyData{linkIndex+1}.vertex.blue]/255;
                h.link(linkIndex+1).Children.FaceColor = 'interp';
            catch ME_1
                disp(ME_1);
                continue;
            end
        end
    end        


    %% Teach
    function Teach(self)
        self.model.teach();
    end  
    %% Logger
    function Log(self,Start,End)
        self.logCount
        tran1=self.model.fkine(Start)
        tran2=self.model.fkine(End)
        tran3=inv(tran1)*tran2
        self.logCount=self.logCount+1;
    end
    
    function GoToXYZ(self,X,Y,Z,R)
        coord=transl(X,Y,Z)
        RobotMove(self,0,coord,25,R)
    end
end
end
