%% Initialise

clc
clear
close all
%

%addpath 'C:\Users\joel-\Documents\robotics_assignment_2'
%run 'C:\Users\joel-\Desktop\assignment 1 Robotics\robot-9.10Small_Modified_20180320_WithVisionTB\robot-9.10Small_Modified\rvctools\startup_rvc.m'
set(0,'DefaultFigureWindowStyle', 'docked')
%% SetUp
% Define Global Variables
stereoTrans=transl(.75,0,0.6);
stereoRot=trotx(0)*troty(-145,'deg')*trotz(90,'deg');
stereoPos=stereoTrans*stereoRot;
baseline=.2;
workspace= [-0.5,0.5,-0.5,0.5,-.1,1.01];
% Distance from robot base to tower base
d = 0.3;
% Width of the Jenga Block
w = 0.0316;
% Height of the Jenga Block
h = 0.029;

stackPoint=[-w,0,w,0,0,0;...
            -d,-d,-d,-d-w,-d,-d+w;... % As it is on the negative Y axis it is negative d
            0.01,0.01,0.01,h,h,h];
        
  
        
% Areas limits for plotting the Jenga parts
xmin1 = -0.22;
xmax1 = -0.05;
xmin2 = 0.05;
xmax2 = 0.22;
ymin = -0.22;
ymax = -0.05;

%Random number in range (a,b) r = a + (b-a).*rand(n,1);
x1 = xmin1 + (xmax1-xmin1).*rand(6,1);
x2 = xmin2 + (xmax2-xmin2)*rand(6,1);
y = ymin + (ymax-ymin).*rand(6,1);
z = 0.01;

% blockLoc=[x1(1,1),x1(2,1),x1(3,1),x2(4,1),x2(5,1),x2(6,1);...
%           y(1,1),y(2,1),y(3,1),y(4,1),y(5,1),y(6,1);...
%           z,z,z,z,z,z;];

blockLoc=[-0.22,-0.14,-0.05,0.16,0.12,0.22;...
          -0.22,-0.16,-0.14,-0.14,-0.22,-0.05;...
          z,z,z,z,z,z;];

% Import Robot (update with DoBot robot class, Rogelio)
bot=dobotmagician(eye(4,4)*trotz(90,'deg'),workspace);
bot.model.plot([0,pi/6,pi/2,pi/3,0], 'scale', .1)

% Import Environment (update with blocks, Rogelio)
hold on
block5=EnvironmentImporter('EnvironmentPly1',0,0,0.01,0,0,270)
% block2=EnvironmentImporter('EnvironmentPly2',0,0,0,0,0,180)
block6=EnvironmentImporter('EnvironmentPly3',0.127,0,0.01,0,0,270)
block7=EnvironmentImporter('EnvironmentPly4',0,0,0.01,0,0,270)
axis (workspace)
%ax.CameraPosition = [0.5 0.5 9]



% Import Blocks and Place Them Randomly
yawmin = -90;
yawmax = 0;
yaw = yawmin + (yawmax-yawmin).*rand(size(blockLoc,2),1);
Iyaw = deg2rad(yaw)+0.7854;

for i = 1:size(blockLoc,2)
    plyname = sprintf('JengaPly%d', i)
    block(i)= partImporter(plyname,blockLoc(1,i),blockLoc(2,i),blockLoc(3,i),0,0,yaw(i,1),workspace)
%    blockLoc(1,i)
%    blockLoc(2,i)
%    blockLoc(3,i)
end



% Import Camera
stereoCam=stereoCamera(baseline,stereoPos)

stereoCam.plotCamera();

% Set Up Lighting
lighting gouraud
light

%% Prereq
bot.prereq(dobot_q)

%% ikine testing
testpos=eye(4,4)*transl(-.19,-.16,0)
testpos2=eye(4,4)*transl(.19,-.16,.2)
q=bot.ikine(testpos)
%q=[90,90,0,0,0]
%bot.model.plot(q)

RobotMove(bot,0,testpos,50)
RobotMove(bot,0,testpos2,50)
%RobotMove(bot,0,testpos,50)
%% Simulation
% Take a photo with the cameras
[camLloc,camRloc]=stereoCam.takePhoto(blockLoc);

% Obtaining the location of the object with the photos

[xcam,ycam,zcam]=stereoCam.findObjectLocCam(blockLoc);% with respect to the Camera coordinate frame.
objectWithRes2Rob=stereoCam.findObjectLocRob(blockLoc,bot);
%PosFromBot = eye(4,4)*trotx(deg2rad(180))
%PosFromBot(1:2,4)=objectWithRes2Rob(1:2,4)

% double checking to ensure the camera location is correct
%check=inv(stereoPos)*eye(4,4)*transl(blockLoc(1),blockLoc(2),blockLoc(3))

%obtain the camera coord
%cameraCoord=transl(xbot,ybot,zbot)

%convert to Dobot coordinate frame
%dobotCoord=stereoRot*cameraCoord+stereoTrans

%RobotMove(bot,0,PosFromBot,50);
%bot.model.fkine(bot.model.getpos())
for i = 1:size(block,2)
    endPt=eye(4,4)*transl([objectWithRes2Rob(1:3,i)'])
    RobotMove(bot,0,endPt,25,Iyaw(i,1),0)
    endPt=eye(4,4)*transl([stackPoint(1:3,i)'])
    if i == 1
        Q5 = atan(w/d);
    elseif i == 2
        Q5 = 0;
    elseif i == 3
        Q5 = -atan(w/d);
    else
        Q5 = pi/2;
    end
    RobotMove(bot,block(i),endPt,25,0,Q5);
end

