%% HardCode Video
blockwidth=0.025;
blockheigh=0.012;
dropvalue=.006;
tableheight=-.059;
returnpt=[.2039,0,-.059+(9*blockheigh),0];
Block(1:4,1)=[.15,    -.09,  tableheight+blockheigh,    0]';
Block(1:4,2)=[.23,    .11,  tableheight+blockheigh,    0]';
Block(1:4,3)=[.35,    -.08,  tableheight+blockheigh,    0]';
Block(1:4,4)=[0.15,    .12,  tableheight+blockheigh,    0]';
Block(1:4,5)=[0.23,    -.11,  tableheight+blockheigh,0]';
Block(1:4,6)=[0.33,    .05,  tableheight+blockheigh,    0]';
FinalLocation(1:4,4)=[.2039,              0,  tableheight+blockheigh,       0]';
FinalLocation(1:4,6)=[.2039+blockwidth,   0,  tableheight+blockheigh,       0]';
FinalLocation(1:4,5)=[.2039-blockwidth,   0,  tableheight+blockheigh,       0]';
FinalLocation(1:4,1)=[.2039,              0,  tableheight+(2*blockheigh),   0]';
FinalLocation(1:4,3)=[.2039,   blockwidth,  tableheight+(2*blockheigh),     0]';
FinalLocation(1:4,2)=[.2039,   -blockwidth,  tableheight+(2*blockheigh),    0]';

for i=1:6
    GoToCartesian( FinalLocation(1,i),FinalLocation(2,i),FinalLocation(3,i),FinalLocation(4,i))
    pause(4)
    SetSuctionCup(1)
    pause(1)
    GoToCartesian(returnpt(1),returnpt(2),returnpt(3),returnpt(4))
    GoToCartesian( Block(1,i),Block(2,i),Block(3,i),Block(4,i))
    %GoToCartesian( Block(1,i),Block(2,i),Block(3,i)-dropvalue,Block(4,i))
    pause(5)
    SetSuctionCup(0)
    
    
   GoToCartesian(returnpt(1),returnpt(2),returnpt(3),returnpt(4))
    %pause
end

function GoToCartesian( x,y,z,r)
            cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos');
            cartmsg_ = rosmessage(cartsvc_);
            cartmsg_.TargetPoints=[x,y,z,r];
            cartsvc_.call(cartmsg_)
end 
function SetSuctionCup(state)
            suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
            suctioncupmsg_ = rosmessage(suctioncupsvc_);
            suctioncupmsg_.IsEndEffectorEnabled=1;
            suctioncupmsg_.EndEffectorState=state;
            suctioncupsvc_.call(suctioncupmsg_);
end
        