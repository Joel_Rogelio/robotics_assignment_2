classdef partImporter < handle
    properties      
        % A cell structure of models
        model;  
        % Dimensions of the workspace
        workspaceDimensions;
        % Ply file data of each environment
        faceData = [];
        vertexData = [];
        plyData = [];
    end
    methods
        %% Creating loading the correct parts into there place
        function self = partImporter(environmentNum,x,y,z,X,Y,Z,workspace)
             %coordinates of object are set when constructing object
             self.model = self.GetModel(environmentNum);
             self.model.base = transl(x,y,z);
             self.model.base = self.model.base()*trotx(X,'deg')*troty(Y,'deg')*trotz(Z,'deg');
             
%             self.model.plot3d(0,'workspace', workspace);
            % Display robot
            
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',workspace, 'delay' , 0);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end  
            self.model.delay = 0;

            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                    catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end     
        
        %% GetModel
        function model = GetModel(self,name)
            if isempty(self.faceData) || isempty(self.vertexData) || isempty(self.plyData)
                [self.faceData,self.vertexData,self.plyData] = plyread([name,'.ply'],'tri');
                %ply file data of environments is stored for each environment
            end
            L1 = Link('alpha',0,'a',1,'d',0,'offset',0);
            model = SerialLink(L1,'name',name);
            %1 link robot used to simulate environments for simplicity
            model.faces = {self.faceData,[]};
            model.points = {self.vertexData,[]}; 
        end
        
    end    
end

      

