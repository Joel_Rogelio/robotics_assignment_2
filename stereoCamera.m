classdef stereoCamera < handle
    properties
        leftCamera;
        leftPos;
        rightCamera;
        rightPos;
        fps=25;
        pose;
        scale=0.15;
        baseline;
    end
methods%% Class for UR3 robot simulation
    function self = stereoCamera(baseline, centrePoint)
        self.pose=centrePoint;
        self.baseline=baseline;
        self.leftPos=centrePoint*transl(-baseline/2,0,0);
        self.rightPos=centrePoint*transl(baseline/2,0,0);
        self.getCameras();
    end

    %% GetCamera
    % Given a name (optional), create and return a Camera
    function getCameras(self)
        % Create left camera
        self.leftCamera=CentralCamera('focal', 0.08, 'pixel', 10e-5, ... %% will need to update bassed on real camera properties.
        'resolution', [1024 1024], 'centre', [512 512],'name', 'LeftCamera','pose',self.leftPos);
        % Create right camera
        self.rightCamera=CentralCamera('focal', 0.08, 'pixel', 10e-5, ... %% will need to update bassed on real camera properties.
        'resolution', [1024 1024], 'centre', [512 512],'name', 'RightCamera','pose',self.rightPos);
    end
    %% PlotCamera
    function plotCamera(self)
        self.leftCamera.T = self.pose;
        self.leftCamera.plot_camera('Tcam',self.leftPos, 'label','scale',self.scale)   
        self.rightCamera.T = self.pose;
        self.rightCamera.plot_camera('Tcam',self.rightPos, 'label','scale',self.scale)  
    end
    %% Take Photo
    function [posL,posR]=takePhoto(self,object)
        self.leftCamera.clf();
        self.leftCamera.plot(object,'Tcam', self.leftPos);
        posL=self.leftCamera.project(object,'Tcam', self.leftPos);
        self.rightCamera.clf();
        self.rightCamera.plot(object,'Tcam', self.rightPos);
        posR=self.rightCamera.project(object,'Tcam', self.rightPos);
    end
    %% Find location of Object With respect to the Camera
    function [x,y,z]=findObjectLocCam(self,object)
        posL=self.leftCamera.project(object,'Tcam', self.leftPos)
        posR=self.rightCamera.project(object,'Tcam', self.rightPos)
        pixelSizeX=self.leftCamera.rho(1);
        pixelSizeY=self.leftCamera.rho(2);
        xl=(posL(1)-self.leftCamera.pp(1))*pixelSizeX
        xr=(posR(1)-self.rightCamera.pp(1))*pixelSizeX
        yl=(posL(2)-self.leftCamera.pp(2))*pixelSizeY
        yr=(posR(2)-self.rightCamera.pp(2))*pixelSizeY
        averagef=(self.leftCamera.f+self.rightCamera.f)/2 % Obtain an
        %average of the 2 cameras focal lengths
        z=(averagef*self.baseline)/(xl-xr);
        x=((xl*(z+averagef))/averagef)-self.baseline/2;
        y=(yl*(z+averagef))/averagef;
    end
       %% Find location of Object with respect to a robot (ie dobot)
    function xyzlocs=findObjectLocRob(self,objectAim,robot)
        posL=self.leftCamera.project(objectAim,'Tcam', self.leftPos);
        posR=self.rightCamera.project(objectAim,'Tcam', self.rightPos);
        pixelSizeX=self.leftCamera.rho(1);
        pixelSizeY=self.leftCamera.rho(2);
        for i = 1:size(posL,2)
            
            xl=(posL(1,i)-self.leftCamera.pp(1))*pixelSizeX;
            xr=(posR(1,i)-self.rightCamera.pp(1))*pixelSizeX;
            yl=(posL(2,i)-self.leftCamera.pp(2))*pixelSizeY;
            yr=(posR(2,i)-self.rightCamera.pp(2))*pixelSizeY;
            averagef=(self.leftCamera.f+self.rightCamera.f)/2; % Obtain an
            %average of the 2 cameras focal lengths
            z=(averagef*self.baseline)/(xl-xr);
            x=((xl*(z+averagef))/averagef)-self.baseline/2;
            y=(yl*(z+averagef))/averagef;
            coordWithResp2Cam=transl(x,y,z);
            robotCoord=robot.model.base()
            camWithResp2Rob=inv(robotCoord)*self.pose
            x=camWithResp2Rob(1,4);
            y=camWithResp2Rob(2,4);
            z=camWithResp2Rob(3,4);
            objWithRes2RobRot=eye(4,4);
            objWithRes2RobRot(1:3,1:3)=camWithResp2Rob(1:3,1:3);
            objWithRes2RobTrans=transl(x,y,z);
            objWithRes2Rob=objWithRes2RobRot*coordWithResp2Cam+objWithRes2RobTrans
            xyzlocs(1:3,i)=objWithRes2Rob(1:3,4)
        end
        
        
    end
end
end