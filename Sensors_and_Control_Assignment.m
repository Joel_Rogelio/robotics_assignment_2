%%ROS INITIALISATION

clear all;
try 
    rosshutdown
end
rosinit('138.25.49.44')

pause(2);

dobotPosCartSub = rossvcclient('/dobot_magician/cart_pos','Timeout', 2);
dobotPosCartMsg = rosmessage(dobotPosCartSub);
dobotStateSub = rossubscriber('/dobot_magician/state');

msg2 = rosmessage('dobot_magician/SetPumpRequest');
eeSrv = rossvcclient('/dobot_magician/pump'); 

msgSetPos = rosmessage('dobot_magician/SetPosAngRequest');
srv = rossvcclient('/dobot_magician/joint_angs');

receive(dobotStateSub,2)

msg = dobotStateSub.LatestMessage;
dobotPosCartMsg.Pos = msg.Pos;

%msgSetPos.JointAngles = [ -0.8422,0.7613,1.0936,0.3000];
%srv.call(msgSetPos);

 dobotPosCartMsg.Pos.X = 0.1681;
 dobotPosCartMsg.Pos.Y = -0.170;
 dobotPosCartMsg.Pos.Z = 0.011;
 dobotPosCartSub.call(dobotPosCartMsg)

pause(1);

%% OBTAINING IMAGE FROM RGB-D CAMERA - REMOTE LAB
rgbRawSub = rossubscriber('/camera/rgb/image_color');   %subscribing to RGB image topic from Asus Xtion Pro
pause(1);
image_h = imshow(readImage(rgbRawSub.LatestMessage));   
image_h.CData = readImage(rgbRawSub.LatestMessage);
drawnow;
imwrite(image_h.CData,'scene.jpg')  %saves the image
sceneArray = imread('scene.jpg');

sceneGrey = rgb2gray(sceneArray);

depthRawSub = rossubscriber('/camera/depth/image'); %subscribing to Depth topic from RGB-D camers
pause(10);
msg = depthRawSub.LatestMessage;
img = readImage(msg);
image_d = imshow(img);

image_d.CData = readImage(depthRawSub.LatestMessage);
drawnow;
imwrite(image_d.CData,'sceneDepth.jpg') %saves depth image
sceneDepthArray = imread('sceneDepth.jpg');

%% OBJECT 1 DETECTION - PLAYING CARD
image = imread('jokerNEW.jpg');

cardObjectGrey = rgb2gray(image);

%sceneimage = imread('sceneGrey.jpg');
%sceneGrey = rgb2gray(sceneimage);
imshow(sceneGrey);

cardObjectPoints = detectSURFFeatures(cardObjectGrey,'MetricThreshold',300);  %default surf parameters 
scenePoints = detectSURFFeatures(sceneGrey,'MetricThreshold',300);

[cardFeatures, cardObjectPoints] = extractFeatures(cardObjectGrey, cardObjectPoints);   %returns features in a usable format - not SURFObjects
[sceneFeatures, scenePoints] = extractFeatures(sceneGrey, scenePoints);

cardPairs = matchFeatures(cardFeatures, sceneFeatures,'MatchThreshold',20); %matches features from object to the scene

matchedCardPoints = cardObjectPoints(cardPairs(:, 1), :);
matchedscenePoints = scenePoints(cardPairs(:, 2), :);

[tform, inlierarrowPoints, inlierscenePoints] = estimateGeometricTransform(matchedCardPoints, matchedscenePoints, 'affine');

figure;
showMatchedFeatures(cardObjectGrey, sceneGrey, inlierarrowPoints, inlierscenePoints,'montage')

Tinv  = tform.invert.T; %inverts the T matrix
ss = Tinv(2,1);
sc = Tinv(1,1);
scaleRecovered = sqrt(ss*ss + sc*sc);  
radiansRecovered = atan2(ss,sc) 

cardPolygon = [1, 1;...                           % top-left
        size(cardObjectGrey, 2), 1;...                 % top-right
        size(cardObjectGrey, 2), size(cardObjectGrey, 1);... % bottom-right
        1, size(cardObjectGrey, 1);...                 % bottom-left
        1, 1];                   % top-left again to close the polygon

newCardPolygon = transformPointsForward(tform, cardPolygon);
cardX=round((newCardPolygon(1, 1)+newCardPolygon(2, 1)+newCardPolygon(3, 1)+newCardPolygon(4, 1))/4);   %calculates centre of the card image in X direction
cardY=round((newCardPolygon(1, 2)+newCardPolygon(2, 2)+newCardPolygon(3, 2)+newCardPolygon(4, 2))/4);   %calculates centre of card image in Y direction

%% OBJECT 2 DETECTION - END ZONE LOCATION
endzone = imread('aceNEW.jpg');
endzoneGrey = rgb2gray(endzone);

endzonePoints = detectSURFFeatures(endzoneGrey);
scenePoints = detectSURFFeatures(sceneGrey);

[endzoneFeatures, endzonePoints] = extractFeatures(endzoneGrey, endzonePoints);
[sceneFeatures, scenePoints] = extractFeatures(sceneGrey, scenePoints);

endzonePairs = matchFeatures(endzoneFeatures, sceneFeatures);

matchEndzonePoints = endzonePoints(endzonePairs(:, 1), :);
matchedscenePoints = scenePoints(endzonePairs(:, 2), :);

figure;
showMatchedFeatures(endzoneGrey, sceneGrey, matchEndzonePoints, ...
    matchedscenePoints, 'montage');
title('Putatively Matched Points (Including Outliers)');

[tform, inlierEndzonePoints, inlierscenePoints] = ...
    estimateGeometricTransform(matchEndzonePoints, matchedscenePoints, 'affine');

figure;
showMatchedFeatures(endzoneGrey, sceneGrey, inlierEndzonePoints, ...
    inlierscenePoints, 'montage');
title('Matched Points (Inliers Only)');

endzonePolygon = [1, 1;...                           % top-left
        size(endzoneGrey, 2), 1;...                 % top-right
        size(endzoneGrey, 2), size(endzoneGrey, 1);... % bottom-right
        1, size(endzoneGrey, 1);...                 % bottom-left
        1, 1];                   % top-left again to close the polygon

newEndzonePolygon = transformPointsForward(tform, endzonePolygon);

Tinv2  = tform.invert.T; %inverts the T matrix
ss2 = Tinv2(2,1);
sc2 = Tinv2(1,1);
scaleRecovered2 = sqrt(ss2*ss2 + sc2*sc2);  
radiansRecovered2 = atan2(ss2,sc2)

endzoneX=round((newEndzonePolygon(1, 1)+newEndzonePolygon(2, 1)+newEndzonePolygon(3, 1)+newEndzonePolygon(4, 1))/4);
endzoneY=round((newEndzonePolygon(1, 2)+newEndzonePolygon(2, 2)+newEndzonePolygon(3, 2)+newEndzonePolygon(4, 2))/4);

figure;
imshow(sceneGrey);
hold on;
plot(cardX,cardY,'r+', 'MarkerSize', 50);
plot(endzoneX,endzoneY,'r+', 'MarkerSize', 50);


%% GET DEPTH INFO
cardDepth = img(cardY,cardX) %we flipped this
endzoneDepth = img(endzoneY,endzoneX)

%% CAMERA PARAMETERS
%Obtained from camera calibration toolbox
focal_length = 534;
ppX = 320;
ppY = 286;

%% CARD ENDZONE LOCATION (CAMERA FRAME)
%Card
XYZCARDcamera=zeros(3,1);
%XYZCARDcamera(3,1)=sqrt((cardDepth^2)/(1+((cardX-ppX)/(focal_length))^2+((cardY-ppY)/(focal_length))^2));
XYZCARDcamera(1,1)=sqrt((cardDepth^2)/(1+((cardX-ppX)/(focal_length))^2+((cardY-ppY)/(focal_length))^2));
%camera z axis = dobot x axis

%XYZCARDcamera(1,1)=((cardX-ppX)*XYZCARDcamera(3,1))/(focal_length);
XYZCARDcamera(2,1)=((cardX-ppX)*XYZCARDcamera(1,1))/(focal_length);
%camera X = dobot Y

%XYZCARDcamera(2,1)=((cardY-ppY)*XYZCARDcamera(3,1))/(focal_length);
XYZCARDcamera(3,1)=((cardY-ppY)*XYZCARDcamera(1,1))/(focal_length);
%camera Y = dobot Z

%endzone
XYZENDZONEcamera=zeros(3,1);
XYZENDZONEcamera(1,1)=sqrt((endzoneDepth^2)/(1+((endzoneX-ppX)/(focal_length))^2+((endzoneY-ppY)/(focal_length))^2));
XYZENDZONEcamera(2,1)=((endzoneX-ppX)*XYZENDZONEcamera(1,1))/(focal_length);
XYZENDZONEcamera(3,1)=((endzoneY-ppY)*XYZENDZONEcamera(1,1))/(focal_length);

%% CAMERA FRAME TO DOBOT FRAME TRANSLATION
TranslationMatrix =  [.465;-.005;.432]; %distance between dobot and camera

RotationY = [-0.656,0,0.7547;0,1,0;-0.7547,0,-0.656];
RotationX = [1,0,0;0,-1,0;0,0,-1];
RotationANS = [-.5479,-.0232,.8362;.0187,.999,.0399;-.8363,.0375,-.5469];

XYZCARDdobot = RotationANS* XYZCARDcamera + TranslationMatrix;

XYZENDZONEdobot = RotationANS* XYZENDZONEcamera + TranslationMatrix;

%% CALCULATE ROTATION
aa=[XYZCARDdobot(1,1),XYZCARDdobot(2,1);XYZENDZONEdobot(1,1),XYZENDZONEdobot(2,1)];
a=pdist(aa,'euclidean');
a2=pdist2([XYZCARDdobot(1,1) XYZCARDdobot(2,1)],[XYZENDZONEdobot(1,1) XYZENDZONEdobot(2,1)], 'euclidean');
bb=[0,0;XYZENDZONEdobot(1,1),XYZENDZONEdobot(2,1)];
b=pdist(bb,'euclidean');
cc=[0,0;XYZCARDdobot(1,1),XYZCARDdobot(2,1)];
c=pdist(cc,'euclidean');
rad=((b^2+c^2-a^2)/(2*b*c))*(3.1415/180);
A=acos(rad);
%%AA=A*(3.1415/180);
%% DOBOT MOVEMENT

% max_X = 0.2948;
% min_X = 0.1848;
% max_Y = 0.2165;
% min_Y = -0.2118;
% max_Z = 0.0874;
% min_Z = 0.0648;

%go to card
dobotPosCartMsg.Pos.X =  XYZCARDdobot(1,1);
dobotPosCartMsg.Pos.Y =  XYZCARDdobot(2,1);
dobotPosCartSub.call(dobotPosCartMsg);
pause(2);
dobotPosCartMsg.Pos.Z =  -.065;
dobotPosCartSub.call(dobotPosCartMsg);
pause(1);
%pump on
msg2.Pump = 1; 
eeSrv.call(msg2);
pause(1);
dobotPosCartMsg.Pos.Z = -.02;
dobotPosCartSub.call(dobotPosCartMsg);
pause(1);
%go to endzone
dobotPosCartMsg.Pos.X =  XYZENDZONEdobot(1,1);
dobotPosCartMsg.Pos.Y =  (XYZENDZONEdobot(2,1));
dobotPosCartSub.call(dobotPosCartMsg);
pause(3);
%Rotation



dobotPosCartMsg.Pos.Z =  -.055;
dobotPosCartSub.call(dobotPosCartMsg);
pause(3);

msg3 = dobotStateSub.LatestMessage;
pause(1);
msgSetPos.JointAngles = msg3.JointAngles + [0;0;0;(radiansRecovered-radiansRecovered2)]
srv.call(msgSetPos)
pause(3);

%pump off
msg2.Pump = 0; 
eeSrv.call(msg2);
pause(1);
dobotPosCartMsg.Pos.Z = -.02;
dobotPosCartSub.call(dobotPosCartMsg);
pause(3);

%move arm out of way
dobotPosCartMsg.Pos.X = 0.22;
dobotPosCartMsg.Pos.Y = -0.100;
dobotPosCartSub.call(dobotPosCartMsg);
pause(2);

dobotPosCartMsg.Pos.X = -0.011;
dobotPosCartMsg.Pos.Y = -0.160;
dobotPosCartSub.call(dobotPosCartMsg);
pause(3);

