%% Movement of robot and parts
% Class for the motion and path planning of a Robot and Desired Parts
% To-Do
%   - include colision avoidance
classdef RobotMove < handle
    methods  
        function self = RobotMove(Robot1,Part1,End1,Steps,IYaw,FYaw)
            if (Part1==0)           % move just the one Robot end no part.
                self.MoveBot(Robot1,End1,Steps,IYaw);
            else                    % move the robot and part
                self.MoveBotandPart(Robot1,Part1,End1,Steps,IYaw,FYaw);
            end
        end
        
        function qMat = Trajectory(self,q1,q2,steps,Robot1)
            s = lspb(0,1,steps);           
            qMat = nan(steps,5);        
            for i = 1:steps
                qMat(i,:) = (1-s(i))*q1 + s(i)*q2; % Trapezoidal Method
            end
        end
        
        function MoveBot(self,Robot1,End1,steps,IYaw) % Function for moving just the robot
            
            robQ=Robot1.model.getpos();
            robQNew=Robot1.ikine(End1);
            robQNew= [robQNew(1,1),robQNew(1,2),robQNew(1,3),robQNew(1,4),IYaw];
            qMat = self.Trajectory(robQ,robQNew,steps,Robot1);
            %Robot1.Log(robQ,robQNew);
            for i = 1:steps
                Robot1.model.animate(qMat(i,:));
                drawnow();
            end
            
        end
        function MoveBotandPart(self,Robot1,Part1,End1,steps,IYaw,FYaw) % Function for moving the robot and the Block
            robQ = Robot1.model.getpos();
            robQNew = [robQ(1,1),pi/6,pi/2,pi/3,robQ(1,5)];
            qMat1 = self.Trajectory(robQ,robQNew,steps,Robot1);
            %Robot1.Log(robQ,robQNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Part1.model.base=Robot1.model.fkine(qMat1(i,:));
                Part1.model.animate(0);
                drawnow();
            end
            
            rob1Q = Robot1.model.getpos();
            rob1QNew = Robot1.ikine(End1);
            rob1QNew = [rob1QNew(1,1),rob1QNew(1,2),rob1QNew(1,3),rob1QNew(1,4),FYaw];
            qMat1 = self.Trajectory(rob1Q,rob1QNew,steps,Robot1);
            %Robot1.Log(rob1Q,rob1QNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Part1.model.base=Robot1.model.fkine(qMat1(i,:));
                Part1.model.animate(0);
                drawnow();
            end
            
            robQ = Robot1.model.getpos();
            robQNew = [robQ(1,1),pi/6,pi/2,pi/3,IYaw];
            qMat = self.Trajectory(robQ,robQNew,steps,Robot1);
            %Robot1.Log(robQ,robQNew);
            for i = 1:steps
                Robot1.model.animate(qMat(i,:));
                drawnow();
            end
            
            
        end
    end     
end