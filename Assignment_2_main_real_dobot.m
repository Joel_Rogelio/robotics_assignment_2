%% Dobot Initialise.
try 
    rosshutdown
    
end
rosinit('http://10.42.0.1:11311')
clear
% Kinect Properties 
load('Kinect_Params.mat')
KinectTrans=transl(.257,-.008,0);
KinectRot=trotz(270,'deg')*trotx(180,'deg');
kinectBase=KinectTrans*KinectRot;
kinectHeight=   0.35;
% Workpace
workspace= [-0.5,0.5,-0.5,0.5,-.1,1.01];
tableHeight=-.150;
doBotReturnPt=[.15,0,-.05];
% BlockDimensions
jengaHeight=     0.019;
jengaWidth=     0.030;%.024
jengaLength=    0.075;
sC=.215;
stackPts=[sC+jengaWidth,sC,sC-jengaWidth,sC,sC,sC;...
         0,0,0,jengaWidth,0,-jengaWidth;...
         tableHeight+(jengaHeight),tableHeight+(jengaHeight),tableHeight+(jengaHeight),tableHeight+2*(jengaHeight),tableHeight+2*(jengaHeight),tableHeight+2*(jengaHeight)];

%% Kinect Camera
Kinect=Kinect(Kinect_Params,kinectBase);
%% DoBot
bot=DoBot_Real(eye(4,4));
%% LocationObtain %% NEED TO CALIBRATE CAMERA AGAIN TO CONFIRM FOCAL LENGTH.
Kinect.TakePhoto();
BlockPos=Kinect.LocateObject('CalibrationPhotos/qr.jpg',   0,  (kinectHeight-jengaHeight),KinectTrans,KinectRot)
bot.GoToXYZ(doBotReturnPt(1), doBotReturnPt(2), doBotReturnPt(3),0)
bot.GoToXYZ(BlockPos(1),BlockPos(2),tableHeight+2*(jengaHeight),0)
pause
bot.GoToXYZ(doBotReturnPt(1), doBotReturnPt(2), doBotReturnPt(3),0)
%% Final Sequence
for i=1:6
    % Go To Return Point
    bot.GoToXYZ(doBotReturnPt(1), doBotReturnPt(2), doBotReturnPt(3),0)
    % Find Block
    Kinect.TakePhoto();
    [BlockPos,rads]=Kinect.LocateObject('CalibrationPhotos/qr.jpg',   0,  (kinectHeight-jengaHeight),KinectTrans,KinectRot)
    % Go To Block (Above)
    bot.GoToXYZ(BlockPos(1,1),BlockPos(2,1),tableHeight+4*(jengaHeight),0)
    % Go To Block
    bot.GoToXYZ(BlockPos(1,1),BlockPos(2,1),tableHeight+(jengaHeight),0)
    % Turn on Suction
    bot.SetSuctionCup(1)
    
    pause(.5)
    if i<=3
        % Go To Return Point
        bot.GoToXYZ(stackPts(1,i),doBotReturnPt(2),doBotReturnPt(3),rads)
        % Go To Stack Point
        bot.GoToXYZ(stackPts(1,i),stackPts(2,i),stackPts(3,i),rads)
    elseif i>3
        % Go To Return Point
        bot.GoToXYZ(sC,stackPts(2,i),doBotReturnPt(3),rads+(pi/2))
        % Go To Stack Point
        bot.GoToXYZ(stackPts(1,i),stackPts(2,i),stackPts(3,i),rads+(pi/2))
    end
    % Turn Off Suction
    pause
    bot.SetSuctionCup(0)
    if i<=3
        % Go To Return Point
        bot.GoToXYZ(stackPts(1,i),doBotReturnPt(2),doBotReturnPt(3),rads)
    elseif i>3
        % Go To Return Point
        bot.GoToXYZ(sC,stackPts(2,i),doBotReturnPt(3),rads+(pi/2))
    end
end
%% BLOCK DETECTION - JENGA
sceneArray = imread('sceneRGB.jpg');

sceneGrey = rgb2gray(sceneArray);

image = imread('qr.jpg');

cardObjectGrey = rgb2gray(image);

%sceneimage = imread('sceneGrey.jpg');
%sceneGrey = rgb2gray(sceneimage);
imshow(sceneGrey);
a=100;
b=100;

        cardObjectPoints = detectSURFFeatures(cardObjectGrey,'MetricThreshold',1000);  %default surf parameters 
        scenePoints = detectSURFFeatures(sceneGrey,'MetricThreshold',1000,'ROI',[400,200,1400,800]);

        [cardFeatures, cardObjectPoints] = extractFeatures(cardObjectGrey, cardObjectPoints);   %returns features in a usable format - not SURFObjects
        [sceneFeatures, scenePoints] = extractFeatures(sceneGrey, scenePoints);

        cardPairs = matchFeatures(cardFeatures, sceneFeatures,'MatchThreshold',20); %matches features from object to the scene

        matchedCardPoints = cardObjectPoints(cardPairs(:, 1), :);
        matchedscenePoints = scenePoints(cardPairs(:, 2), :);

        [tform, inlierarrowPoints, inlierscenePoints] = estimateGeometricTransform(matchedCardPoints, matchedscenePoints, 'affine');


figure;
showMatchedFeatures(cardObjectGrey, sceneGrey, inlierarrowPoints, inlierscenePoints,'montage')

Tinv  = tform.invert.T; %inverts the T matrix
ss = Tinv(2,1);
sc = Tinv(1,1);
scaleRecovered = sqrt(ss*ss + sc*sc);  
radiansRecovered = atan2(ss,sc) 

cardPolygon = [1, 1;...                           % top-left
        size(cardObjectGrey, 2), 1;...                 % top-right
        size(cardObjectGrey, 2), size(cardObjectGrey, 1);... % bottom-right
        1, size(cardObjectGrey, 1);...                 % bottom-left
        1, 1];                   % top-left again to close the polygon

newCardPolygon = transformPointsForward(tform, cardPolygon);
cardX=round((newCardPolygon(1, 1)+newCardPolygon(2, 1)+newCardPolygon(3, 1)+newCardPolygon(4, 1))/4);   %calculates centre of the card image in X direction
cardY=round((newCardPolygon(1, 2)+newCardPolygon(2, 2)+newCardPolygon(3, 2)+newCardPolygon(4, 2))/4);   %calculates centre of card image in Y direction

%% Muck around
GoToCartesian(.2039,0,-.059,0);
%SetGripper(false)
%SetSuctionCup(1)
CurrentJointState(0)

%% DoBot Functions
function GoToCartesian( x,y,z,r)
            cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos');
            cartmsg_ = rosmessage(cartsvc_);
            cartmsg_.TargetPoints=[x,y,z,r];
            cartsvc_.call(cartmsg_)
        end  
        function GoToJointAngles(q1,q2,q3,q4)
            jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles');
            jangmsg_ = rosmessage(jangsvc_);
            jangmsg_.TargetPoints = [ q1, q2, q3, q4 ];
            jangsvc_.call(jangmsg_);
        end
        function SetSuctionCup(state)
            suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
            suctioncupmsg_ = rosmessage(suctioncupsvc_);
            suctioncupmsg_.IsEndEffectorEnabled=1;
            suctioncupmsg_.EndEffectorState=state;
            suctioncupsvc_.call(suctioncupmsg_);
        end
        function SetGripper(state)
            grippersvc_ = rossvcclient('/dobot_magician/end_effector/set_gripper');
            grippermsg_ = rosmessage(grippersvc_);
            grippermsg_.IsEndEffectorEnabled=0;
            grippermsg_.EndEffectorState=state;
            grippersvc_.call(grippermsg_);
        end
        function CurrentJointState(q)
            sub = rossubscriber('/dobot_magician/joint_states');
            pause(1);
            %receive and print latest state message
            disp 'Receiving and printing latest state message ...'
            statemsg_ = receive(sub,10); %timeout after 10 seconds
            disp (statemsg_.Position)
        end