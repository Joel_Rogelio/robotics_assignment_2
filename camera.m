classdef camera < handle
    properties
        cam;
        fps=25;
        pose;
        scale=0.15;
    end
methods%% Class for UR3 robot simulation
    function self = camera(base)
        self.pose=base;
        self.getCamera();
    end

    %% GetCamera
    % Given a name (optional), create and return a Camera
    function getCamera(self)
        self.cam=CentralCamera('focal', 0.08, 'pixel', 10e-5, ... %% will need to update bassed on real camera properties.
        'resolution', [1024 1024], 'centre', [512 512],'name', 'Camera','pose',self.pose);
        
    end
    %% PlotCamera
    function plotCamera(self)
        self.cam.T = self.pose;
        self.cam.plot_camera('Tcam',self.pose, 'label','scale',self.scale)        
    end
    %% Take Photo
    function pos=takePhoto(self,object)
        self.cam.clf();
        self.cam.plot(object);
        pos=self.cam.project(object);
    end
    
end
end