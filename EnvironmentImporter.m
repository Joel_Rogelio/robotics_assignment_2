classdef EnvironmentImporter < handle
    properties      
        % A cell structure of models
        model;  
        % Dimensions of the workspace
        workspaceDimensions;
        % Ply file data of each environment
        faceData = [];
        vertexData = [];
        plyData = [];
    end
    methods
        %% Creating loading the correct parts into there place
        function self = EnvironmentImporter(environmentNum,x,y,z,X,Y,Z)
            [f,v,data] = plyread([environmentNum,'.ply'],'tri');
            % Get vertex count
            VertexCount = size(v,1);
            % Create a transform to describe the location (at the origin,
            % since it's centered)
            Pose = eye(4);
            vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
            Mesh_h = trisurf(f,v(:,1),v(:,2),v(:,3) ...
                ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
            Pose = Pose * transl(x,y,z) * trotx(X,'deg')*troty(Y,'deg')*trotz(Z,'deg');
            updatedPoints = [Pose * [v,ones(VertexCount,1)]']';
            Mesh_h.Vertices = updatedPoints(:,1:3);
        end
       
    end    
end