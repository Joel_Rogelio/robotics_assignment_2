classdef UR3 < handle
    properties
        %> Robot model
        model;
        logCount=1;
        
              
      
    end
    
methods%% Class for UR3 robot simulation
    function self = UR3(base)

    %> Define the boundaries of the workspace

    % robot = 
    self.GetUR3Robot(base);
    % robot = 
    %self.PlotAndColourRobot(); %robot,workspace);
    end

    %% GetUR3Robot
    % Given a name (optional), create and return a UR3 robot model
    function GetUR3Robot(self,base)
    %     if nargin < 1
            % Create a unique name (ms timestamp after 1ms pause)
            pause(0.001);
            name = ['UR_3_',datestr(now,'yyyymmddTHHMMSSFFF')];
    %     end

        L1 = Link('d',0.1519,   'a',0,          'alpha',pi/2,   'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        L2 = Link('d',0.11985,  'a',-0.24365,   'alpha',0,      'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        L3 = Link('d',-0.09321, 'a',-0.21325,   'alpha',0,      'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        %L4 = Link('d',0.11235,  'a',0,          'alpha',pi/2,   'offset',0,     'qlim',0);
        L4 = Link('d',0.0852,   'a',0,          'alpha',pi/2,   'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        L5 = Link('d',0.08535,  'a',0,          'alpha',-pi/2,  'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);
        %L6 = Link('d',0.0819,   'a',0,          'alpha',0,      'offset',0,     'qlim',0);
        L6 = Link('d',.0819,        'a',0,          'alpha',0,      'offset',0,     'qlim',[deg2rad(-360), deg2rad(360)]);

        self.model = SerialLink([L1 L2 L3 L4 L5 L6],'name',name);
        self.model.base = base;
    end
    %% PlotAndColourRobot
    % Given a robot index, add the glyphs (vertices and faces) and
    % colour them in if data is available 
    function PlotAndColourRobot(self,workspace)%robot,workspace)
        for linkIndex = 0:self.model.n

                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['UR3Link',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
            self.model.faces{linkIndex+1} = faceData;
            self.model.points{linkIndex+1} = vertexData;
        end

        % Display robot
        self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',workspace, 'delay' , 0);
        if isempty(findobj(get(gca,'Children'),'Type','Light'))
            camlight
        end  
        self.model.delay = 0;
        hold on;

        % Try to correctly colour the arm (if colours are in ply file data)
        for linkIndex = 0:self.model.n
            handles = findobj('Tag', self.model.name);
            h = get(handles,'UserData');
            try 
                h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                              , plyData{linkIndex+1}.vertex.green ...
                                                              , plyData{linkIndex+1}.vertex.blue]/255;
                h.link(linkIndex+1).Children.FaceColor = 'interp';
            catch ME_1
                disp(ME_1);
                continue;
            end
        end
    end        


    %% Teach
    function Teach(self)
        self.model.teach();
    end  
    %% Logger
    function Log(self,Start,End)
        self.logCount
        tran1=self.model.fkine(Start)
        tran2=self.model.fkine(End)
        tran3=inv(tran1)*tran2
        self.logCount=self.logCount+1;
    end
end
end
