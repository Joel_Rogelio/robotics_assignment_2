%% Movement of robot and parts
classdef RobotMove < handle
    methods  
        function self = RobotMove(Robot1,Robot2,Part1,Part2,Part3,End1,End2,Steps)
            if (Robot2==0&Part1==0&Part2==0&End2==0&Part3==0)%move just the one Robot end no part.
                self.MoveBot(Robot1,End1,Steps);
            elseif (Part1==0&Part2==0&Part3==0) %move both robots no parts
                self.MoveBots(Robot1,Robot2,End1,End2,Steps);
            elseif (Robot2==0&Part2==0&End2==0&Part3==0)
                self.MoveBotandPart(Robot1,Part1,End1,Steps)
            elseif (Robot2==0&End2==0)
                self.MoveBotandParts(Robot1,Part1,Part2,Part3,End1,Steps);              
            else %move both robots and both parts
                self.MoveBotsandParts(Robot1,Robot2,Part1,Part2,End1,End2,Steps);
            end  
        end
        
        function qMat = Trajectory(self,q1,q2,steps)
            s = lspb(0,1,steps);           
            qMat = nan(steps,6);        
            for i = 1:steps
                qMat(i,:) = (1-s(i))*q1 + s(i)*q2; 
            end
        end
        
        function MoveBot(self,Robot1,trans,steps)   
            robQ = Robot1.model.getpos();
            robQNew = Robot1.model.ikcon(trans);
            qMat = self.Trajectory(robQ,robQNew,steps);
            Robot1.Log(robQ,robQNew);
            for i = 1:steps
                Robot1.model.animate(qMat(i,:));
                drawnow();
            end
        end
        function MoveBotandPart(self,Robot1,Part1,End1,steps)
            rob1Q = Robot1.model.getpos();
            rob1QNew = Robot1.model.ikcon(End1);
            qMat1 = self.Trajectory(rob1Q,rob1QNew,steps);
            Robot1.Log(rob1Q,rob1QNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Part1.model.base=Robot1.model.fkine(qMat1(i,:));
                Part1.model.animate(0);
                drawnow();
            end   
        end
               
        function MoveBots(self,Robot1,Robot2,End1,End2,steps)
            rob1Q = Robot1.model.getpos();
            rob1QNew = Robot1.model.ikcon(End1);
            rob2Q = Robot2.model.getpos();
            rob2QNew = Robot2.model.ikcon(End2);
            qMat1 = self.Trajectory(rob1Q,rob1QNew,steps);
            qMat2 = self.Trajectory(rob2Q,rob2QNew,steps);
            Robot1.Log(rob1Q,rob1QNew);
            Robot2.Log(rob2Q,rob2QNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Robot2.model.animate(qMat2(i,:));
                drawnow();
            end
            
        end
        function MoveBotsandParts(self,Robot1,Robot2,Part1,Part2,End1,End2,steps)
            rob1Q = Robot1.model.getpos();
            rob1QNew = Robot1.model.ikcon(End1);
            rob2Q = Robot2.model.getpos();
            rob2QNew = Robot2.model.ikcon(End2);
            qMat1 = self.Trajectory(rob1Q,rob1QNew,steps);
            qMat2 = self.Trajectory(rob2Q,rob2QNew,steps);
            Robot1.Log(rob1Q,rob1QNew);
            Robot2.Log(rob2Q,rob2QNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Robot2.model.animate(qMat2(i,:));
                Part1.model.base=Robot1.model.fkine(qMat1(i,:));
                Part2.model.base=Robot2.model.fkine(qMat2(i,:));
                Part1.model.animate(0);
                Part2.model.animate(0);
                drawnow();
            end     
        end
        function MoveBotandParts(self,Robot1,Part1,Part2,Part3,End1,steps)
            rob1Q = Robot1.model.getpos();
            rob1QNew = Robot1.model.ikcon(End1);
            qMat1 = self.Trajectory(rob1Q,rob1QNew,steps);
            Robot1.Log(rob1Q,rob1QNew);
            for i = 1:steps
                Robot1.model.animate(qMat1(i,:));
                Part1.model.base=Robot1.model.fkine(qMat1(i,:));
                Part2.model.base=Robot1.model.fkine(qMat1(i,:));
                Part3.model.base=Robot1.model.fkine(qMat1(i,:));
                Part1.model.animate(0);
                Part2.model.animate(0);
                Part3.model.animate(0);
                drawnow();
            end  
        end
        
    end     
end