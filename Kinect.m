% Need to Load Calibrated Camera Results As "Kinect_Params" Before Calling This Class !!!!
classdef Kinect < handle
    properties
        PP;
        FL;
        Image;
        Base;
        pixelSizeX;
        pixelSizeY;
    end
    methods
        function self = Kinect(Kinect_Params,Base)
            self.PP=Kinect_Params.PrincipalPoint
            self.FL=Kinect_Params.FocalLength
            self.Base=Base;
            self.pixelSizeX=0;
            self.pixelSizeY=0;
        end
        function RGBPhoto = TakePhoto(self)
            RGBCam = videoinput('kinect', 1, 'BGR_1920x1080'); % Kinect RGB Camera
            unflippedRGBframe = getsnapshot(RGBCam);
            RGBframe=flipdim(unflippedRGBframe,2); %flip image
            imshow(RGBframe)
            image(RGBframe);
            imwrite(RGBframe,'C:\Users\joel-\Documents\robotics_assignment_2\robotics_assignment_2\RGB.jpg');
            self.Image=RGBframe;
            RGBPhoto=RGBframe;
        end
        function [objResp2Rob,radiansRecovered]=LocateObject(self,object,scene,z,kinectTrans,kinectRot) %This Function only works when the camera is looking directly
            if scene==0                            %down on the environment and the distance between the camera 
                scene=self.Image;                  %and the top of the blocks is know (z).
            else
                scene = imread(scene);
            end

            sceneGrey = rgb2gray(scene);
            indicesW = find(sceneGrey>20);
            sceneGrey(indicesW) = sceneGrey(indicesW)+30;
            indicesB = find(sceneGrey<20);
            sceneGrey(indicesB) = sceneGrey(indicesB) - 10;
            sceneGrey(1:620,800:1100)=0;
            % Adjust contrast
            
            
            %
            image = imread(object);

            objectGrey = rgb2gray(image);
%            indicesW = find(objectGrey>20);
%            objectGrey(indicesW) = objectGrey(indicesW)+50;
%            indicesB = find(objectGrey<20);
%            objectGrey(indicesB) = objectGrey(indicesB) - 10;
a=300;
b=100;
da=1400;
db=700;
areas=9;
areaWidth=da/3;
areaHeight=db/3;

%for x=1:areas/3
%    for y=1:areas/3
%        for xa=1:2
%            for yb=1:2
%                try
%                    startX=a+(x-1)*areaWidth+(xa-1)*(areaWidth/2)
%                    startY=b+(y-1)*areaHeight+(xa-1)*(areaHeight/2)
                    objectPoints = detectSURFFeatures(objectGrey,'MetricThreshold',1000);  %default surf parameters 
                    scenePoints = detectSURFFeatures(sceneGrey,'MetricThreshold',1000,'ROI',[300,100,1400,700]);%[startX,startY,areaWidth,areaHeight]);%[300,100,1400,700]

                    [objectFeatures, objectPoints] = extractFeatures(objectGrey, objectPoints);   %returns features in a usable format - not SURFObjects
                    [sceneFeatures, scenePoints] = extractFeatures(sceneGrey, scenePoints);

                    pairs = matchFeatures(objectFeatures, sceneFeatures,'MatchThreshold',40); %matches features from object to the scene

                    matchedObjectPoints = objectPoints(pairs(:, 1), :);
                    matchedscenePoints = scenePoints(pairs(:, 2), :);

                    [tform, inlierarrowPoints, inlierscenePoints] = estimateGeometricTransform(matchedObjectPoints, matchedscenePoints, 'affine');
%                end
%            end
%        end
%    end      
%end

            figure;
            showMatchedFeatures(objectGrey, sceneGrey, inlierarrowPoints, inlierscenePoints,'montage')

            Tinv  = tform.invert.T; %inverts the T matrix
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scaleRecovered = sqrt(ss*ss + sc*sc);  
            radiansRecovered = atan2(ss,sc) 

            objectPolygon = [1, 1;...                           % top-left
            size(objectGrey, 2), 1;...                 % top-right
            size(objectGrey, 2), size(objectGrey, 1);... % bottom-right
            1, size(objectGrey, 1);...                 % bottom-left
            1, 1];                   % top-left again to close the polygon

            newobjectPolygon = transformPointsForward(tform, objectPolygon);
            objectX=round((newobjectPolygon(1, 1)+newobjectPolygon(2, 1)+newobjectPolygon(3, 1)+newobjectPolygon(4, 1))/4);   %calculates centre of the card image in X direction
            objectY=1080-round((newobjectPolygon(1, 2)+newobjectPolygon(2, 2)+newobjectPolygon(3, 2)+newobjectPolygon(4, 2))/4);   %calculates centre of card image in Y direction
            locationInImage=[objectX,objectY,radiansRecovered]';
            U=-(objectX-self.PP(1)); % Negative to invert the X axis
            V=objectY-self.PP(2);
            thetaX=atan(U/self.FL(1,1));
            thetaY=atan(V/self.FL(1,2));
            X=z*tan(thetaX);
            Y=z*tan(thetaY);
            Z=z;
            objResp2Cam=[X,Y,Z]';
            rpy=tr2rpy(self.Base);
            kinectRot=kinectRot(1:3,1:3);
            kinectTrans=kinectTrans(1:3,4);
            radiansRecovered
            if radiansRecovered<=-pi/2&&radiansRecovered>-pi
                radiansRecovered= (pi+radiansRecovered)
            elseif radiansRecovered<=0&&radiansRecovered>-pi/2
                radiansRecovered;
            elseif radiansRecovered>0&&radiansRecovered<pi/2
                radiansRecovered;
            elseif radiansRecovered>=pi/2&&radiansRecovered<pi
                radiansRecovered=-(pi-radiansRecovered)
            end 
            objResp2Rob=(kinectRot*objResp2Cam)+kinectTrans;
        end
    end
end
